
# deploy new openstack swift object storage cluster 
ansible-playbook -i inventories/staging_host -u thienln --ask-become-pass --key-file /home/thienln/.ssh/id_rsa  main-playbook.yml

# sync new ring files to all nodes 
ansible-playbook -i inventories/staging_host -u thienln --ask-become-pass --key-file /home/thienln/.ssh/id_rsa  playbook/swift-sync-ring.yml

# restart all swift services
ansible-playbook -i inventories/staging_host -u thienln --ask-become-pass --key-file /home/thienln/.ssh/id_rsa  playbook/swift-restart-all.yml

